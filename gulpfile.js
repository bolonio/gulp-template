'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var include = require('gulp-include');
var minify = require('gulp-cssnano');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var gutil = require('gulp-util');
var config = require('./config.json');

gulp.task('css', function(){
  return gulp.src(config.styles.src)
    .pipe(minify({ keepBreaks: true }))
    //.pipe(rename({ suffix: '.min' }))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(config.styles.dest))
    .pipe(reload({stream:true}));
});

gulp.task('less', function(){
  return gulp.src(config.less.src)
    .pipe(less())
    .pipe(minify({ keepBreaks: true }))
    .pipe(gulp.dest(config.less.dest))
    .pipe(reload({stream:true}));
});

gulp.task('js', function () {
  gulp.src(config.scripts.libraries)
    //.pipe(uglify())
    .pipe(gulp.dest(config.scripts.dest));

  return gulp.src(config.scripts.src)
    .pipe(uglify())
    //.pipe(concat('script.js'))
    .pipe(gulp.dest(config.scripts.dest))
    .pipe(reload({stream:true}));
});

gulp.task('html', function(){
  return gulp.src(config.html.src)
    .pipe(plumber())
    .pipe(gulp.dest(config.html.dest))
    .pipe(reload({stream:true}));
});

gulp.task('images', function(){
  return gulp.src(config.images.src)
    .pipe(gulp.dest(config.images.dest))
    .pipe(reload({stream:true}));
});

gulp.task('fonts', function(){
  return gulp.src(config.fonts.src)
    .pipe(gulp.dest(config.fonts.dest))
    .pipe(reload({stream:true}));
});

gulp.task('browser-sync', function() {
  browserSync.init({
    port: 8080,
    host: 'gulp.dev',
    server: {
      baseDir: config.dest
    }
  });
});

gulp.task('watch', function(){
  gulp.watch([config.styles.libraries], ['css']);
  gulp.watch([config.less.src], ['less']);
  gulp.watch([config.scripts.src], ['js']);
  gulp.watch([config.html.src], ['html']);
  gulp.watch([config.images.src], ['images']);
  gulp.watch([config.fonts.src], ['fonts']);
});

gulp.task('default', ['css', 'less', 'js', 'html', 'images','fonts', 'browser-sync', 'watch']);