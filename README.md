# Template for gulp

This is a template or a initial setup for a simple gulp project. It aims to provide some common tasks to a web app development within a organized structure.

## Getting things up and running
- Install [Node.js](http://nodejs.org)

```
 $ git clone https://github.com/bolonio/gulp-template.git
 $ cd gulp-template
 $ npm install
```

## Development guidelines
#### Project structure
```
├── src
│   └── styles
│   │   └── main.less
│   └── scripts
│   └── images
│   └── fonts
│   └── index.html
├── dist
│   └── styles
│   │   └── main.min.css
│   └── scripts
│   │   └── scripts.min.js
│   └── images
│   └── fonts
│   └── index.html
└── node_modules
```

## Run gulp
### Run gulp with browserSync server
```
  $ gulp
```

##License
Copyright (c) 2016 [Adrián Bolonio](https://github.com/bolonio/) Licensed under [the MIT license](https://github.com/bolonio/gulp-template/blob/master/LICENSE.md).
